#!/usr/bin/make -f

UPSTREAM_GIT := https://github.com/eventlet/eventlet
include /usr/share/openstack-pkg-tools/pkgos.make

export PYBUILD_NAME=eventlet
export TRAVIS=true

export SETUPTOOLS_SCM_PRETEND_VERSION=$(shell dpkg-parsechangelog -SVersion | sed -e 's/^[[:digit:]]*://' -e 's/[-].*//' -e 's/~git.*//' -e 's/~bpo.*//' -e 's/~/.0/' -e 's/+dfsg1//' -e 's/+ds1//' | head -n 1)

%:
	dh $@ --with python3,sphinxdoc --buildsystem=pybuild

override_dh_auto_clean:
	dh_auto_clean
	rm -f hatch.toml eventlet/_version.py
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_auto_build:
	echo "__version__ = '$$SETUPTOOLS_SCM_PRETEND_VERSION'" >eventlet/_version.py
	dh_auto_build

override_dh_fixperms:
	dh_fixperms
	echo "__version__ = '$$SETUPTOOLS_SCM_PRETEND_VERSION'" >$(CURDIR)/debian/python3-eventlet/usr/lib/python3/dist-packages/eventlet/_version.py

override_dh_sphinxdoc:
	PYTHONPATH=. python3 -m sphinx -N -b html doc/source $(CURDIR)/debian/python-eventlet-doc/usr/share/doc/python-eventlet-doc/html
	dh_sphinxdoc

override_dh_compress:
	dh_compress -X.inv -X.txt

override_dh_installchangelogs:
	dh_installchangelogs NEWS

override_dh_auto_test:
ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	# Backlist 4 unit tests depending on /etc/resolv.conf:
	# test_clear
	# test_noraise_dns_tcp
	# test_raise_dns_tcp
	# test_dns_methods_are_green
	# See upstream issue: https://github.com/eventlet/eventlet/issues/963
	# Non-deterministic:
	# test_direct_raise_instance
	# test_cancel_proportion'
	# See #1082997
	# test_orig_thread
	# See #1090270
	set -e ; set -x ; for i in $(shell py3versions -vr 2>/dev/null) ; do \
		PYTHONPATH=. PYTHON=python$$i python$$i -m pytest tests -v -n `nproc` -k ' not test_fork_after_monkey_patch and not test_cancel_proportion and not test_clear and not test_noraise_dns_tcp and not test_raise_dns_tcp and not test_dns_methods_are_green and not test_orig_thread and not test_send_1k_pub_sub and not test_ssl_close' ; \
	done
endif
